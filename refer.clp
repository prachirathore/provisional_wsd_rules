;##############################################################################
;#  Copyright (C) 2013-2014 Anita Chaturvedi (anita@iiit.ac.in)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;In her autobiography she occasionally refers to her unhappy schooldays.
;अपनी आत्मकथा में उसने कई बार उसके स्कूल के दुख से भरे दिनों का उल्लेख किया है .
(defrule refer1
(declare (salience 4700))
(id-root ?id refer)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 autobiography)
(kriyA-in_saMbanXI  ?id ?)
;(kriyA-kriyA_viSeRaNa  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refer.clp 	refer1   "  ?id "  ulleKa_kara )" crlf))
)
;"refer","V","2.saMxarBa_xe"
(defrule refer2
;(declare (salience 4900))
(id-root ?id refer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkewa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refer.clp 	refer2   "  ?id "  saMkewa_kara )" crlf))
)

;"refer","VTI","1.saMkewa_karanA"
;Does this remark refer to you?  
;--"2.sOMpanA"
;The matter has been referred to the committee.
;--"3.havAlA_xenA"
;The advocate frequently referred to his notes.
;He always refers to the house as his "refuge".
;वह अपने घर को हमेशा “आश्रय-स्थल” कहकर हवाला देता है। 
(defrule refer3
(declare (salience 4500))
(id-root ?id refer)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id havAlA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refer.clp 	refer3   "  ?id "  havAlA_xe )" crlf))
)

;My doctor referred me to a hospital specialist.  [cambridge Dictionary]
;मेरे डाक्टर ने मुझे अस्पताल के विशेषज्ञ से परामर्श हेतु भेजा ।
;You should refer a patient to a specialist for treatment.  [cambridge Dictionary]
;आपको मरीज़ को विशेषज्ञ से परामर्श हेतु भेजना चाहिए ।
;--"4.parAmarSa_hewu_BejanA"
(defrule refer4
(declare (salience 4300))
(id-root ?id refer)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 specialist)
(kriyA-to_saMbanXI  ?id ?id1)
;(kriyA-subject  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parAmarSa_hewu_Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refer.clp 	refer4   "  ?id "  parAmarSa_hewu_Beja)" crlf))
)

;#####################defaultrule################################

(defrule refer0
;(declare (salience 5000))
(id-root ?id refer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxarBa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refer.clp 	refer0   "  ?id "  saMxarBa_xe )" crlf))
)
