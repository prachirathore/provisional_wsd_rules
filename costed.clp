;##############################################################################
;#  Copyright (C) 2002-2005 Preeti Pradhan (pradhan.preet@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;(31-10-13)
;Have you costed out these proposals yet?[ Oxford Advanced Learner's Dictionary]
;kyA Apane aba waka ina praswAvoM kA  mUlya_AMkA  hE?
(defrule costed1
(declare (salience 1000))
(id-word ?id costed)
(id-word ?id1 out) 
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mUlya_AMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  costed.clp	costed1   "  ?id "  " ?id1"  mUlya_AZka )" crlf))
)

;#############################Defaults rule#######################################
;(31-10-13)
;The building work has been costed at $30,000.[ Oxford Advanced Learner's Dictionary]
;imArawa kArya kA 30,000 dAlara mUlya AMkA gayA hE.
(defrule costed0
(declare (salience 900))
(id-word ?id costed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya_AMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  costed.clp	costed0   "  ?id "  mUlya_AZka )" crlf))
)

;################### Additional Examples ####################
;The project needs to be costed in detail.
;Their accountants have costed the project at $8.1 million.
;Have you costed out these proposals yet?
