;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;Do you think Dad will allow you to go to Jamie's party?
;क्या तुम्हें लगता है कि पिता जी तुम्हें जैमी कीं पार्टी में जाने कीं  देंगे ?
(defrule allow0
(declare (salience 4000))
(id-root ?id allow)
?mng <-(meaning_to_be_decided ?id) 
(kriyA-vAkyakarma  ?id ?id1)
(to-infinitive  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumawi_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allow.clp 	allow0   "  ?id "  anumawi_xe )" crlf)
)
)

;You're not allowed to talk during the exam.
;परीक्षा के समय आपको बात करने कीं इजाजत नहीं है. 
(defrule allow1
(declare (salience 3500))
(id-root ?id allow)
?mng <-(meaning_to_be_decided ?id) 
(kriyA-kriyArWa_kriyA  ?id ?id1)
(to-infinitive  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumawi))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allow.clp 	allow1   "  ?id "  anumawi )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* " allow.clp    allow0   "  ?id " ko )" crlf)
)
)

;**************************DEFAULT RULES*************************************

(defrule allow2
(declare (salience 0))
(id-root ?id allow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allow.clp 	allow2  "  ?id "  anumawi )" crlf))
)






