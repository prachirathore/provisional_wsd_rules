;I tied up my luggage with a string.
;मैंने रस्सी से अपना सामान बांधा .
(defrule string0
(declare (salience 5000))
(id-root ?id string)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rassI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  string.clp 	string0   "  ?id "  rassI )" crlf))
)


;Her fingers are running on guitar's string like an expert gitarist.
;उसकी उंगलिया  एक निपुण  गिटार वादक की तरह गिटार के तार पर चल रही हैं.
;added by Rashmi Ranjan
(defrule string3
(declare (salience 5000))
(id-root ?id string)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 guitar|cello|violin|piano)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_wAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  string.clp 	string3   "  ?id "  kA_wAra )" crlf))
)


;The band had a string of hits in the 1990s.
;1990 के दशक में  बैंड के पास हिट की श्रृंखला थी.
;A long string of names.
;नामों की एक लंबी श्रृंखला.
;A string of robberies.
;डकैती की एक श्रृंखला.
;A computer has two strings of number.
;कंप्यूटर के पास अंको की दो श्रृंखला होती है.
;Added by Rashmi Ranjan
(defrule string2
(declare (salience 5000))
(id-root ?id string)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SrqMKalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  string.clp 	string2   "  ?id "  SrqMKalA )" crlf))
)


;"string","N","1.rassI"
;I tied up my luggage with a string.
;--"2.gitAra_yA_vayolina_kA_wAra"
;Her fingers are running on gitar's string like an expert gitarist.
;--"3.SrqMKalA"
;A string of imported beads is used to make the necklace.
;--"4.guNoM_kI_SrufKalA"

;modified by Rashmi Ranjan
(defrule string1
(declare (salience 4900))
(id-root ?id string)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  string.clp 	string1   "  ?id "  bAzXA_huA )" crlf))
)

;"string","V","1.ko_rassI_se_bAzXanA"
; Lamps were string on the pole.
;--"2.Cote_Cote_cIjZoM_ko_sUwra_meM_PironA"
;Fibers strung together to form rope.
;
