
(defrule end0
(declare (salience 5000))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " end.clp	end0  "  ?id "  " ?id1 "  pUrA_ho  )" crlf))
)

;He ended up in the hospital carelessness.
;vaha lAparavAhI kI vajaha se haspawAla meM pUrA ho gayA
(defrule end1
(declare (salience 4900))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) far)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA_Cora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end1   "  ?id "  xUsarA_Cora )" crlf))
)

;The house is at the far end of ..
(defrule end2
(declare (salience 4800))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " end.clp	end2  "  ?id "  " ?id1 "  samApwa_ho  )" crlf))
)

(defrule end3
(declare (salience 4700))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " end.clp	end3  "  ?id "  " ?id1 "  samApwa_ho  )" crlf))
)

(defrule end4
(declare (salience 4600))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end4   "  ?id "  samApwa_ho )" crlf))
)

;For a finite large planar sheet, Eq. (1.33) is approximately true in the middle regions of the planar sheet, away 
;from the ends.
;किसी परिमित बडी समतलीय चादर के लिए समीकरण ( 1.33 ) , सिरों से दूर समतलीय चादर के मध्यवर्ती क्षेत्रों में सन्निकटतः सत्य है . 

;Added by Pramila(Banasthai University) on 23-10-2013
(defrule end5
(declare (salience 4600))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sirA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end5   "  ?id "  sirA )" crlf))
)

;Added by Pramila(Banasthali University) on 28-10-2013
;She came to an untimely end.  ;oald
;usakI asAmayika mqwyu huI.
;He met his end at battle of waterloo.  ;oald
;vAtaralU kI ladAI meM usakI mqwyu ho gaI.
(defrule end6
(declare (salience 5000))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-to_saMbanXI  ?id1 ?id)(kriyA-object  ?id1 ?id))
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mqwyu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end6   "  ?id "  mqwyu )" crlf))
)


;Added by Pramila(Banasthali University) on 28-10-2013
;He joined the society for political ends. ;oald
;vaha rAjanIwika uxxeSya se saMsWA meM sammiliwa huA..
(defrule end7
(declare (salience 5000))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(kriyA-for_saMbanXI  ?id2 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end7   "  ?id "  uxxeSya )" crlf))
)

;----------------------default rules-----------------------------
;This is the end of the street.
;yaha galI kA anwa hE.
(defrule end8
(declare (salience 4000))
(id-root ?id end)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  end.clp 	end8   "  ?id "  anwa )" crlf))
)

